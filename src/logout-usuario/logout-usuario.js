import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class LogoutUsuario extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <button on-click="logout">Por favor, Confirme que desear abandonar sesion</button>
     <span hidden$="[[!isLogout]]">Hasta pronto!</span>


     <iron-ajax
       id="doLogout"
       url="http://localhost:3000/apitechu/v2/logout/{{userid}}"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     userid : {
       type: Number,
       observer: "_useridChanged"
     }, accounts: {
       type: Array
     }, isLogout: {
       type: Boolean,
       value: false
     }
   };
 } // End properties

 logout() {
   console.log("El usuario ha pulsado el botón");
   console.log("idUser: " + this.userid)

   var logoutData = {
     "idUser": this.userid
   }

   console.log("logoutData " + logoutData)

   this.$.doLogout.body = JSON.stringify(logoutData);
   this.$.doLogout.generateRequest();

 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.body);

   this.isLogout = true;
  this.dispatchEvent(new CustomEvent("eventologout", {"detail":{"userid":this.userid}}));

 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

} // End class

window.customElements.define('logout-usuario', LogoutUsuario);
