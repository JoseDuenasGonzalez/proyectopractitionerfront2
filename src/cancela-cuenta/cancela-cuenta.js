import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
* @customElement
* @polymer
*/
class CancelaCuenta extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>Cancelar Cuenta</h1>
     <h3>IBAN - {{iban}}</h3>


      <button disabled$="[[isCancel]]" on-click="cancelAccount">Pulse para confirmar la cancelación</button>



      <button on-click="volverCanCuentas">Volver a Cuentas</button>

      <span hidden$="[[!isCancel]]">Cuenta cancelada correctamente</span>

     <iron-ajax
       id="doCancel"
       url="http://localhost:3000/apitechu/v2/accounts/{{iban}}"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError">
     </iron-ajax>

   `;
 }


 static get properties() {
   return {
     iban : {
       type: String,
       observer: "_ibanChanged"
     }, movements: {
       type: Array
     }, isCancel: {
       type: Boolean,
       value: false,
       observer: '_isCancelChanged'
     }
   };
 }

 cancelAccount() {
   console.log("El usuario ha pulsado el botón");
   console.log("iban: " + this.iban)



   var altaData = {
     "IBAN": this.iban
   }

   console.log("altaData " + altaData)

   this.$.doCancel.body = JSON.stringify(altaData);
   this.$.doCancel.generateRequest();
 }

 _ibanChanged() {
    this.$.doCancel.generateRequest();
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.body);

   this.isCancel = true;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

 _isCancelChanged(isCancel) {
   console.log("isCancel ha cambiado" + this.isCancel);

 }

 volverCanCuentas(e) {
  console.log("volverCanCuentas");
  this.isCancel = false;
  this.dispatchEvent(new CustomEvent("vueltacancuentas"));

 }

}

window.customElements.define('cancela-cuenta', CancelaCuenta);
