import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class CambioDivisa extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <h1>Consulta tu cambio en divisa</h1>
     <select value="{{currency::input}}">
       <option>Seleccionar divisa destino</option>
       <option value="JPY">YEN</option>
       <option value="USD">Dolar</option>
       <option value="GBP">Libra</option>
     </select>
     <br />
     <br />
     <input type="amount" placeholder="amount" value="{{amount::input}}" />
     <br />
     <br />
     <button on-click="cambio">Pulse para obtener su importe en la divisa solicitada</button>

     <button on-click="volverCuentas">Volver a Cuentas</button>
     </br>
     </br>
     <span hidden$="[[!isCambio]]">Su balance en esta divisa es de: {{balanceSal}}</span>
     </br>
     </br>
     <div id="msgCambDiv"></div>


     <iron-ajax
       id="doCambio"
       url="http://localhost:3000/apitechu/v1/currencys/{{currency}}/{{amount}}"
       handle-as="json"
       content-type="application/json"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     isCambio: {
       type: Boolean,
       value: false,
       observer: '_isCambioChanged',
     }, balance: {
       type: Number
     }, balanceSal: {
       type: Number
     }, currency: {
       type: String
     }, amount: {
       type: Number
     }
   };
 } // End properties

 cambio() {
   console.log("El usuario ha pulsado el botón");
   this.isCambio = false;
   console.log("curency: " + this.currency)
   console.log("amount: " + this.amount)
   console.log("validarDatos en cambio: " + this.validarDatos)

   if (!this.validarDatos()) {
     console.log("entra en el if de validarDatos")
     this.currency = null;
     this.amount = null;
     this.shadowRoot.getElementById("msgCambDiv").style.color = "#ff0000";
     this.shadowRoot.getElementById("msgCambDiv").innerHTML = "Se ha producido un <strong> ERROR. </strong> Por favor, asegúrate de haber introducido todos los datos y de que el importe sea numérico";
   } else {

      this.$.doCambio.generateRequest();
     }
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log("amount" + data.detail.response.result.amount);
   console.log("target" + data.detail.response.result.target);
   console.log("source" + data.detail.response.result.source);
   console.log("quantity" + data.detail.response.result.quantity);

   this.shadowRoot.getElementById("msgCambDiv").innerHTML = "";

   this.balanceSal = data.detail.response.result.amount

   this.isCambio = true;
   this.currency = null;
   this.amount = null;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

 volverCuentas(e) {
  console.log("volverCuentas");
  this.isCambio = false;
  this.shadowRoot.getElementById("msgCambDiv").innerHTML = "";

  this.dispatchEvent(new CustomEvent("vueltadivcuentas"));

 }

 _isCambioChanged(isCambio) {
   console.log("isCambio ha cambiado" + this.isCambio);

   this.$.doCambio.generateRequest();

 }

 _isCambioChanged(isCambio) {
   console.log("isCambio ha cambiado" + this.isCambio);

 }

 validarDatos() {
  console.log("entra a validar datos")
  var retorno = true;
  if (((this.amount == 0) || (this.amount == "")) ||
     ((this.currency == null) || (this.currency == ""))) {
      console.log("entra a validar nulos")
      retorno = false;
  }

    var regDecimal = /^\d*(\.\d{1})?\d{0,1}$/;
    console.log("el importe " + this.amount)
    console.log("validacion decimal " + regDecimal.test(this.amount))
    if (!regDecimal.test(this.amount)) {
        console.log("entra a validar importe")
        retorno = false;
    }

    return(retorno);
  }
} // End class

window.customElements.define('cambio-divisa', CambioDivisa);
