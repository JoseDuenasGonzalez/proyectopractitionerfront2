import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class AltaUsuario extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <input type="email" placeholder="email" value="{{email::input}}" />
     <br />
     <input type="password" placeholder="password" value="{{password::input}}" />
     <br />
     <input type="first_name" placeholder="first_name" value="{{first_name::input}}" />
     <br />
     <input type="last_name" placeholder="last_name" value="{{last_name::input}}" />
     <br />
     <button disabled$="[[isAlta]]" on-click="alta">Alta</button>
     <button hidden$="[[!isAlta]]" on-click="irLogin">Pulsa para acceder a tu nueva cuenta</button>
     <span hidden$="[[!isAlta]]">Enhorabuena! te has dado de alta como usuario</span>
     <br />
     <div id="msgAltaUsu"></div>


     <iron-ajax
       id="doAlta"
       url="http://localhost:3000/apitechu/v2/users"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     password: {
       type: String
     }, email: {
       type: String
     }, isAlta: {
       type: Boolean,
       value: false
     }, first_name: {
       type: String
     }, last_name: {
       type: String
     }
   };
 } // End properties

 alta() {
   console.log("El usuario ha pulsado el botón");

   if (!this.validarDatos()) {
     console.log("error de email");
     this.shadowRoot.getElementById("msgAltaUsu").style.color = "#ff0000";
     this.shadowRoot.getElementById("msgAltaUsu").innerHTML = "<strong> Por favor, introduce un Email correcto </strong> ";

   } else {

       var altaData = {
         "email": this.email,
         "password": this.password,
         "first_name": this.first_name,
         "last_name": this.last_name
       }

       console.log("altaData " + this.email)

       this.$.doAlta.body = JSON.stringify(altaData);
       this.$.doAlta.generateRequest();
    }
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.email);

   this.isAlta = true;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log("Error " + error.detail.request.__data.status);
   console.log(error.detail.request.xhr.response);
   console.log("El error es: " + error.detail.request.xhr.response.msg);
   if (error.detail.request.__data.status == 401) {
      console.log("El usuario ya existe");
        this.shadowRoot.getElementById("msgAltaUsu").style.color = "#ff0000";
        this.shadowRoot.getElementById("msgAltaUsu").innerHTML = "Usuario ya existe";
    } else {
        console.log('ERROR GENERAL');
        this.shadowRoot.getElementById("msgAltaUsu").style.color = "#ff0000";
        this.shadowRoot.getElementById("msgAltaUsu").innerHTML = "Se ha producido un ERROR en el alta";
      }
 }

 irLogin(e) {
  console.log("irLogin");
  this.isAlta = false;
  this.email = null
  this.password = null
  this.first_name = null
  this.last_name = null
  this.dispatchEvent(new CustomEvent("iralogin"));

 }

 validarDatos() {
  console.log("entra a validar datos")
  var retorno = true;
  if (((this.email == null) || (this.email == "")) ||
     ((this.password == 0) || (this.password == "")) ||
     ((this.first_name == 0) || (this.first_name == ""))||
     ((this.last_name == 0) || (this.last_name == ""))) {
      console.log("entra a validar nulos")
      retorno = false;
  }
 }
} // End class

window.customElements.define('alta-usuario', AltaUsuario);
