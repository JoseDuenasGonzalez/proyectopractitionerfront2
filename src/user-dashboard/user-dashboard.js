import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '../visor-cuenta/visor-cuenta.js';
import '../logout-usuario/logout-usuario.js';
import '../alta-cuenta/alta-cuenta.js';
import '../visor-movimientos/visor-movimientos.js';
import '../alta-movimiento/alta-movimiento.js';
import '../cancela-cuenta/cancela-cuenta.js';
import '../cambio-divisa/cambio-divisa.js';
import '@polymer/iron-pages/iron-pages.js'
import '@polymer/iron-selector/iron-selector.js'
import '@polymer/app-route/app-route.js'
import '@polymer/app-route/app-location.js'

/**
* @customElement
* @polymer
*/
class UserDashboard extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
       h1 {
        background-color: #869251;
        }
        .heading { color: #FBFCFC; font-weight: bold; }

     </style>
     <font color="Olive" face="Comic Sans MS,arial">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1 class="heading">JMD BANK</h1>
     <br />
     <pre><h3 hidden$="{{!isLogged}}">Cliente: {{nombre}} {{apellido}}        Nombre de usuario: {{emailCabecera}} </h3></pre>
     <h4 hidden$="{{isLogged}}">Date de ALTA para empezar a operar con tu Banco Online, si ya eres cliente pulsa LOGIN</h4>

     <br />
     <br />

     <button hidden$="{{isLogged}}" on-click="showLogin">Login Cliente</button>
     <button hidden$="{{isLogged}}" on-click="showAlta">Alta Cliente</button>
     <button hidden$="{{!isLogged}}" on-click="showLogout">Logout</button>
     <button hidden$="{{!isLogged}}" on-click="showCuentas">Consultar cuentas</button>


     <br />
     <br />

     <iron-pages
        selected="[[page]]"
        attr-for-selected="name"
        role="main">
      <login-usuario name="login" on-loginsucess="processLoginSuccess"></login-usuario>
      <visor-usuario name="usuario" userid={{userid}} on-eventocuentas="consultarCuentas" on-eventoaltacuenta="altaCuenta" on-usersucess="datosUsuario"></visor-usuario>
      <visor-cuenta name="cuenta" userid={{userid}} flag={{flag}} on-eventomovimientos="consultarMovimientos" on-eventoaltacuenta="altaCuenta" on-eventocancelar="cancelarCuenta" on-cambdivisa="cambiarDivisa"></visor-cuenta>
      <alta-usuario name="alta" on-iralogin="showLogin"></alta-usuario>
      <logout-usuario name="logout" userid={{userid}}></logout-usuario>
      <alta-cuenta name="altacuenta" userid={{userid}} on-vueltacuentas="volver"></alta-cuenta>
      <visor-movimientos name="movimientos" iban={{iban}} flagmov={{flagmov}} on-altamovimiento="altaMovimiento" on-vueltamovcuentas="volverMovCuenta"></visor-movimientos>
      <cambio-divisa name="cambio" on-vueltadivcuentas="volverDivCuenta"></cambio-divisa>
      <alta-movimiento name="pagealtamovimiento" iban={{iban}} on-vueltamovimientos="volverMov"></alta-movimiento>
      <cancela-cuenta name="cancelacuenta" iban={{iban}} on-vueltacancuentas="volverCanCuenta"></alta-movimiento>

    </iron-pages>
    `;
  }

  static get properties() {
    return {

  //*    consultarCuentas2(e) {
  //*      console.log("funcion consultarCuenta");
  //*      this.balance = e.detail.idUser;
  //*      console.log("id:" + this.idUser);
  //*      this.page = "cuenta";
  //*    },

      page: {
        type: String,
        reflectToAttribute: true,
      },
      isLogged: {
        type : Boolean,
        value : false,
        reflectToAttribute: true,
        observer: '_isLoggedChanged',
      },
      flag: {
        type : Number,
        value : +0,
      },
      flagmov: {
        type : Number,
        value : +0,
      },
      nombre: {
        type : String,
        reflectToAttribute: true,
        observer: '_isNombreChanged',
      },
      apellido: {
        type : String,
        reflectToAttribute: true,
        observer: '_isApellidoChanged',
      },
      emailCabecera: {
        type : String,
        reflectToAttribute: true,
        observer: '_isEmailChanged',
      }

    };
  }

  _isLoggedChanged(isLogged) {
    console.log("isLogged ha cambiado" + this.isLogged);

  }

  _isNombreChanged(nombre) {
    console.log("nombre ha cambiado" + this.nombre);

  }

  _isApellidoChanged(nombre) {
    console.log("apellido ha cambiado" + this.apellido);

  }

  _isEmailChanged(nombre) {
    console.log("email ha cambiado" + this.emailCabecera);

  }

  showLogin(e) {
    console.log("showLogin");
    this.page = "login";

  }

  showAlta(e) {
    console.log("showAlta");
    this.page = "alta";

  }

  showLogout(e) {
    console.log("showLogout");
    console.log(this.userid);
    this.isLogged = false;
    console.log("isLogged:" + this.isLogged);
    this.page = "logout";

  }

  showCuentas(e) {
    console.log("showCuentas");
    console.log(this.userid);
    console.log("isLogged:" + this.isLogged);
    this.flag += 1;
    this.page = "cuenta";

  }

  processLoginSuccess(e) {
    console.log("processLoginSuccess");
    console.log(e.detail.idUser);
    this.isLogged = true;
    console.log("isLogged:" + this.isLogged);
    this.userid = e.detail.idUser;
    this.page = "usuario";

  }

  consultarCuentas(e) {
    console.log("funcion consultarCuenta");
    console.log("id:" + this.userid);
    this.page = "cuenta";
  }

  consultarMovimientos(e) {
    console.log("funcion consultarMovimientos");
    console.log("iban:" + e.detail.IBAN);
    this.flagmov += 1;
    this.iban = e.detail.IBAN;
    this.page = "movimientos";
  }

  altaCuenta(e) {
    console.log("funcion altaCuenta");
    console.log(String(this.userid));
    this.page = "altacuenta";
  }

  cancelarCuenta(e) {
    console.log("funcion cancelarCuenta");
    console.log("iban: " + e.detail.IBAN );
    this.iban = e.detail.IBAN;
    console.log(String(this.iban));
    this.page = "cancelacuenta";
  }

  altaMovimiento(e) {
    console.log("funcion altaMovimiento");
    console.log(String(this.iban));
    this.page = "pagealtamovimiento";
  }

  volver(e) {
    console.log("funcion volver");
    this.flag += 1;
    console.log("flag:" + this.flag);
    this.page = "cuenta";
  }

  volverMov(e) {
    console.log("funcion volverMov");
    this.flagmov += 1;
    console.log("flagmov:" + this.flagmov);
    this.page = "movimientos";
  }

  volverMovCuenta(e) {
    console.log("funcion volverMovCuenta");
    this.flag += 1;
    console.log("flag:" + this.flag);
    this.page = "cuenta";
  }

  volverCanCuenta(e) {
    console.log("funcion volverCanCuenta");
    this.flag += 1;
    console.log("flag:" + this.flag);
    this.page = "cuenta";
  }

  volverDivCuenta(e) {
    console.log("funcion volverDivCuenta");
    this.page = "cuenta";
  }

  datosUsuario(e) {
    console.log("datosUsuario");
    console.log(e.detail.first_name);
    console.log(e.detail.last_name);
    console.log(e.detail.email);
    this.nombre = e.detail.first_name;
    this.apellido = e.detail.last_name;
    this.emailCabecera = e.detail.email;
  }

  cambiarDivisa(e) {
    console.log("funcion cambiarDivisa");
    this.page = "cambio";
  }

 }

 window.customElements.define("user-dashboard", UserDashboard);
