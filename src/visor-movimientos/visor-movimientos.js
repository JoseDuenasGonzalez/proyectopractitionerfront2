import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
* @customElement
* @polymer
*/
class VisorMovimientos extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
       .heading { color: #FBFCFC; }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>Sus movimientos</h1>
     <h3>IBAN - {{iban}}</h3>
     <table cellspacing="10" cellpadding="10" border="3" width="900">
      <tr>
        <th bgcolor="DBE99F" style="width:300px">Tipo de Movimiento</th>
        <th bgcolor="DBE99F" style="width:300px">Fecha</th>
        <th bgcolor="DBE99F" style="width:300px">Importe</th>
      </tr>
    </table>

      <dom-repeat items="[[movements]]">
       <template>

         <table hidden$="{{noMov}}" cellspacing="10" cellpadding="10" border="1" width="900">

          <tr>
            <td bgcolor="DBE99F" style="width:300px">{{item.movement_type}}</td>
            <td bgcolor="DBE99F" style="width:300px">{{item.date}}</td>
            <td bgcolor="DBE99F" style="width:300px">{{item.amount}}</td>
          </tr>

        </table>
       </template>
      </dom-repeat>

      <button on-click="addMovements">Realizar Nuevo Movimiento</button>
      <button on-click="volverMovCuentas">Volver a Cuentas</button>

     <iron-ajax
       id="getMovements"
       url="http://localhost:3000/apitechu/v2/movements/{{iban}}"
       handle-as="json"
       on-response="showData"
       on-error="showError">
     </iron-ajax>

     <iron-ajax auto
        id="refreshMovement"
        url="http://localhost:3000/apitechu/v2/movements/{{iban}}"
        handle-as="json"
        on-response="showData"
        on-error="showError">
   `;
 }


 static get properties() {
   return {
     iban : {
       type: String,
       observer: "_ibanChanged"
     }, movements: {
       type: Array
     },
     flagmov : {
       type : Number,
       value: +0,
       observer: 'refresh'
     }, noMov: {
       type: Boolean,
       value: true,
       observer: '_noMovChanged',
     }
   };
 }

 showData(data) {
   console.log(data.detail.response);
   this.noMov = false;
   this.movements = data.detail.response;
 }

 _ibanChanged() {
    this.$.getMovements.generateRequest();
 }

 addMovements(){
   console.log("iban de visor movimientos " + this.iban);
   this.dispatchEvent(new CustomEvent("altamovimiento", {"detail":{"iban":this.iban}}));

 }

 refresh(flagmov) {
    console.log("entro en refreshmov");
    console.log("iban:" + this.iban);
    this.$.refreshMovement.generateRequest();
 }

 volverMovCuentas(e) {
  console.log("volverMovCuentas");
  this.dispatchEvent(new CustomEvent("vueltamovcuentas"));

 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
   this.noMov = true;
 }

 _noMovChanged(noMov) {
   console.log("noMov ha cambiado" + this.noMov);

 }
}

window.customElements.define('visor-movimientos', VisorMovimientos);
