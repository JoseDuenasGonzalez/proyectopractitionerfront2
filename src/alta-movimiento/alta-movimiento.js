import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class AltaMovimiento extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h3>ALTA movimientos</h1>
     <h4>IBAN - {{iban}}</h3>
     <select value="{{type::input}}">
       <option>Seleccionar tipo de movimiento</option>
       <option value="INGRESO">INGRESO</option>
       <option value="REINTEGRO">REINTEGRO</option>
     </select>
     <br />
     <br />
     <input type="date" placeholder="date" value="{{date::input}}" required/>
     <br />
     <br />
     <input id="amount" type="amount" placeholder="amount" value="{{amount::input}}" />
     <br />
     <br />
     <button on-click="alta">Confirmar movimiento</button>

     <button on-click="volverMovimientos">Volver a Movimientos</button>
     </br>
     <span hidden$="[[!isAlta]]">Movimiento realizado correctamente</span>
     <div id="msgAltaMov"></div>


     <iron-ajax
       id="doAlta"
       url="http://localhost:3000/apitechu/v2/movements"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     iban: {
       type: String
     }, isAlta: {
       type: Boolean,
       value: false,
       observer: '_isAltaChanged',
     }, type: {
       type: String
     }, amount: {
       type: Number
     }, amount: {
       type: String
     }
   };
 } // End properties

 alta() {
   console.log("El usuario ha pulsado el botón");
   this.isAlta = false;
   console.log("iban: " + this.iban)

   if (!this.validarDatos()) {
     this.shadowRoot.getElementById("msgAltaMov").style.color = "#ff0000";
     this.shadowRoot.getElementById("msgAltaMov").innerHTML = "Se ha producido un <strong> ERROR. </strong> Por favor, asegúrate de haber introducido todos los datos y de que el importe sea numérico";
   } else {

         var altaData = {
           "IBAN": this.iban,
           "movement_type": this.type,
           "date": this.date,
           "amount": parseFloat(this.amount)
         }

         console.log("altaData " + this.altaData)

         this.$.doAlta.body = JSON.stringify(altaData);
         this.$.doAlta.generateRequest();
       }
    }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.altaData);

   this.isAlta = true;
   this.type = null;
   this.amount = null;
   this.date = null;
   this.shadowRoot.getElementById("msgAltaMov").innerHTML = "";


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

 volverMovimientos(e) {
  console.log("volverMovimientos");
  this.isAlta = false;
  this.shadowRoot.getElementById("msgAltaMov").innerHTML = "";
  this.dispatchEvent(new CustomEvent("vueltamovimientos"));

 }

 _isAltaChanged(isAlta) {
   console.log("isAlta ha cambiado" + this.isAlta);

 }

 validarDatos() {
  console.log("entra a validar datos")
  var retorno = true;
  if (((this.type == null) || (this.type == "")) ||
     ((this.date == 0) || (this.date == "")) ||
     ((this.amount == 0) || (this.amount == ""))) {
      console.log("entra a validar nulos")
      retorno = false;
  }

  //var regDecimal = /\d{0,9}(\.\d{0,2})?/
  var regDecimal = /^\d*(\.\d{1})?\d{0,1}$/;
  console.log("el importe " + this.amount)
  console.log("validacion decimal " + regDecimal.test(this.amount))
  if (!regDecimal.test(this.amount)) {
      console.log("entra a validar importe")
      retorno = false;
  }

  return(retorno);

}

} // End class

window.customElements.define('alta-movimiento', AltaMovimiento);
