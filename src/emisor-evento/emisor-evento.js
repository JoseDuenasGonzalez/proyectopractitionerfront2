import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h3>Soy el emisor</h3>
      <button on-click="sendEvent">No pulsar</button>
    `;
  }
  static get properties() {
    return {
    };
  } // End properties
  sendEvent(e) {
    console.log("Botón pulsado");
    console.log(e);
// definimos el evento que queremos devolver, lo llamamos CustomEvent,
//y en este caso devolvemos un object llamado myevent
    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "course" : "TechU",
            "year" : 2019
          }
        }
      )
    )
  }
} // End class

window.customElements.define('emisor-evento', EmisorEvento);
