import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
* @customElement
* @polymer
*/
class VisorCuenta extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>Sus cuentas</h1>


     <table cellspacing="10" cellpadding="10" border="3" width="900">
      <tr>
        <th bgcolor="DBE99F" style="width:300px">IBAN</th>
        <th bgcolor="DBE99F" style="width:300px">Balance</th>
        <th bgcolor="DBE99F" style="width:300px">Acción</th>
      </tr>
    </table>

       <template
       is="dom-repeat"
       items="{{accounts}}"
       as="account">


         <table cellspacing="10" cellpadding="10" border="1" width="900">

          <tr>
            <td bgcolor="DBE99F" style="width:300px">{{account.IBAN}}</td>
            <td bgcolor="DBE99F" style="width:300px">{{account.balance}}</td>
            <td bgcolor="DBE99F">
                 <button style="width:220px" name="accounts" id="{{account.IBAN}}" on-click="showMovements" value="{{account.IBAN::input}}">Ver movimientos</button>
                 <button style="width:220px" name="cancel" id="{{account.IBAN}}" on-click="cancelAccount" value="{{account.IBAN::input}}">Cancelar la cuenta</button>
                 <button style="width:220px" name="cambio" on-click="cambiDivisa">Consultar divisas</button>
            </td>
          </tr>

        </table>
       </template>

       <button on-click="addAccounts">Alta nueva cuenta</button>

     <iron-ajax auto
       id="getAccounts"
       url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
       handle-as="json"
       on-response="showData">
     </iron-ajax>

     <iron-ajax auto
        id="refreshAccount"
        url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
        handle-as="json"
        on-response="showData"
    >
   `;
 }

 static get properties() {
   return {
     userid : {
       type: String,
       observer: "_useridChanged"
     }, accounts: {
       type: Array
     },
     flag : {
       type : Number,
       value: +0,
       observer: 'refresh'
     }
   };
 }

 showData(data) {
   console.log(data.detail.response);
   this.accounts = data.detail.response;

 }

 showMovements(e){
   console.log("Entro en consulta de movimiento");
   console.log("e.target " + e.target.value);
   var desIban = e.target.value;
   console.log("desIban:" + desIban);
   this.dispatchEvent(new CustomEvent("eventomovimientos", {"detail":{"IBAN":desIban}}));

 }

 cancelAccount(e){
   console.log("Entro en cancelar la cuenta");
   console.log("e.target " + e.target.value);
   var desIban = e.target.value;
   console.log("desIban:" + desIban);
   this.dispatchEvent(new CustomEvent("eventocancelar", {"detail":{"IBAN":desIban}}));

 }

 _useridChanged() {
    this.$.getAccounts.generateRequest();
 }

 addAccounts(){
   console.log("userid de visor cuenta " + this.userid);
   this.dispatchEvent(new CustomEvent("eventoaltacuenta", {"detail":{"userid":this.userid}}));

 }

 cambiDivisa(){
   console.log("Entramos en cambio de divisa");
   this.dispatchEvent(new CustomEvent("cambdivisa"));

 }

 refresh(flag) {
    console.log("entro en refresh");
    console.log("id:" + this.userid);
    console.log("iban:" + this.iban);
    this.$.refreshAccount.generateRequest();
 }
}

window.customElements.define('visor-cuenta', VisorCuenta);
