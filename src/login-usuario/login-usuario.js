import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class LoginUsuario extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
       .boton {
            padding:20px;
            background:#502121;
            color:white;
            font:bold 10px/10px verdana;
            width:100px;
            cursor:pointer;
            cursor:hand;
            text-align:center;
        }

        #boton1 {
          position:absolute;
          left: 25%;

        }

        #boton2 {
            position:absolute;
            left: 25%;

        }
     </style>
     <br />
     <br />
     <input type="email" placeholder="email" value="{{email::input}}" />
     <br />
     <br />
     <input type="password" placeholder="password" value="{{password::input}}" />
     <br />
     <br />
     <button on-click="login">Login</button>


     <div id="msglogin"></div>


     <iron-ajax
       id="doLogin"
       url="http://localhost:3000/apitechu/v2/login"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     password: {
       type: String
     }, email: {
       type: String
     }, isLogged: {
       type: Boolean,
       value: false
     }
   };
 } // End properties

 login() {
   console.log("El usuario ha pulsado el botón");

   var validaEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
   if (!validaEmail.test(this.email)) {

     console.log("error de email");
     this.shadowRoot.getElementById("msglogin").style.color = "#ff0000";
     this.shadowRoot.getElementById("msglogin").innerHTML = "<strong> Por favor, introduce un Email correcto </strong> ";

   } else {

     var loginData = {
       "email": this.email,
       "password": this.password
     }

     console.log("loginData " + this.email)

     this.$.doLogin.body = JSON.stringify(loginData);
     this.$.doLogin.generateRequest();
  }
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.email);

   this.isLogged = true;
   this.email = null;
   this.password = null;

   this.dispatchEvent(
     new CustomEvent(
       "loginsucess",
       {
         "detail" : {
           "idUser" : data.detail.response.idUser
         }
       }
     )
   )
 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);

   this.email = null;
   this.password = null;
   this.shadowRoot.getElementById("msglogin").style.color = "#ff0000";
   this.shadowRoot.getElementById("msglogin").innerHTML = "<strong> Usuario o Password incorrecto </strong> ";

 }

} // End class

window.customElements.define('login-usuario', LoginUsuario);
