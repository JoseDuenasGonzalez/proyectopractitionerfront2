import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class AltaCuenta extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <button disabled$="[[isAlta]]" on-click="alta">Por favor, Confirme que quiere crear su nueva cuenta</button>

     <button on-click="volverCuentas">Volver a Cuentas</button>
     </br>
     </br>
     <span hidden$="[[!isAlta]]">La cuenta se ha dado de alta correctamente</span>


     <iron-ajax
       id="doAlta"
       url="http://localhost:3000/apitechu/v2/accounts"
       handle-as="json"
       content-type="application/json"
       method="POST"
       on-response="manageAJAXResponse"
       on-error="showError"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     iban: {
       type: String
     }, isAlta: {
       type: Boolean,
       value: false,
       observer: '_isAltaChanged',
     }, iduser: {
       type: String
     }, balance: {
       type: Number
     }
   };
 } // End properties

 alta() {
   console.log("El usuario ha pulsado el botón");
   console.log("iduser: " + this.userid)

   var altaData = {
     "IBAN": " ",
     "idUser": this.userid,
     "balance": 0
   }

   console.log("altaData " + this.altaData)

   this.$.doAlta.body = JSON.stringify(altaData);
   this.$.doAlta.generateRequest();
 }

 manageAJAXResponse(data) {
   console.log("Llegaron los resultados");
   console.log(data.detail.response);
   console.log(data.detail.response.altaData);

   this.isAlta = true;


 }

 showError(error) {
   console.log("Hubo un error");
   console.log(error);
   console.log(error.detail.request.xhr.response);
 }

 volverCuentas(e) {
  console.log("volverCuentas");
  this.isAlta = false;
  this.dispatchEvent(new CustomEvent("vueltacuentas"));

 }

 _isAltaChanged(isAlta) {
   console.log("isAlta ha cambiado" + this.isAlta);

 }

} // End class

window.customElements.define('alta-cuenta', AltaCuenta);
